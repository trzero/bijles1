# Taak 1

![Voorbeeld01](./Voorbeeld01.png)

Zoals besproken in de les graag onderstaande uitvoeren.
Schrijf de index,style en script voor onderstaande resultaat te bekomen.

- Let erop dat de code jouw eigen is
- Foto’s zijn grey’d-out tot de muis er over komt
- Zowel de pijltjes als de minifoto’s kunnen gebruikt worden om naar een andere slide te gaan.

Deadline: 18/12; zodat we dit nog kunnen bespreken in de les.
