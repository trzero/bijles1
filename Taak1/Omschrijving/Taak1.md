# Taak 1

Webpagina; navigatie (5 elementen) waarvan minimum 2 kunnen uitklappen

# Taak 2

1 daar van gaat naar een carrousel met minstens 5 foto’s die automatisch lopen

# Extra

Laat deze klok verlopen per keer dat er wordt opgeklikt.
`<i class="fa-solid fa-alarm-clock"></i> <i class="fa-solid fa-alarm-exclamation"></i> <i class="fa-solid fa-alarm-plus"></i> <i class="fa-solid fa-alarm-snooze"></i>`

![voorbeeld](./Voorbeeld01.png)
