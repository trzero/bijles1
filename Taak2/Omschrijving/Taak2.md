# Taak 1

Zorg ervoor dat deze vier statistieken werken:
![voorbeeld](./voorbeeld01.png)
Creëer een vijfde stat, die zal weergeven wat onze win/los ratio is.

# Taak 2

Maak een website waar je de keuze hebt om bladsteenschaar te spelen tegen een AI of tegen een speler twee.
Zorg dat beide duidelijk gescheiden zijn.

# Extra

Kijk om een systeem te maken zodat als er bijvoorbeeld na 30 seconden geen reactie van ‘player twee’ komt, de AI overneemt.
